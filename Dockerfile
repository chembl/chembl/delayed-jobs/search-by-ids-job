ARG INSTALL_PYTHON_VERSION=${INSTALL_PYTHON_VERSION:-3.8}
FROM python:${INSTALL_PYTHON_VERSION}-slim-buster AS base
ARG USERNAME=${USERNAME:-glados}
ARG UID=${UID:-123}
ARG GID=${GID:-321}

SHELL ["/bin/bash", "-c"]
RUN apt-get update

WORKDIR /app
COPY . .

RUN pip install --upgrade pip

RUN useradd -m ${USERNAME} -u ${UID}
RUN chown -R ${UID}:${GID} /app
USER ${USERNAME}
ENV PATH="/home/${USERNAME}/.local/bin:${PATH}"

RUN pip install virtualenv
RUN virtualenv /app/python_env
RUN source /app/python_env/bin/activate; pip install -r requirements.txt

ENTRYPOINT functional_tests/run_tests.sh