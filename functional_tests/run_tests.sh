#!/usr/bin/env bash

set -x

PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/delete_test_subset_tasks.py

## ----------------------------------------------------------------------------------------------------------------------
## Simulate a simple job with the ids list in the parameters from molecule ChEMBL IDs to ChEMBL Molecules
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_1 || true
#mkdir -p $PWD/functional_tests/output_1
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_1.yml -n
#EXIT_CODE_1=$?
#
#if [[ $EXIT_CODE_1 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_1/matches.json
#ls -lah $PWD/functional_tests/output_1/summary.json
#
## ----------------------------------------------------------------------------------------------------------------------
## Simulate a simple job with the ids list in the parameters:
## - some ids are from other entity
## - some ids do not exist at all in ChEMBL
## - some ids are repeated
## - some ids are obsolete
## - some ids are inactive
## from molecule ChEMBL IDs to ChEMBL Molecules
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_2 || true
#mkdir -p $PWD/functional_tests/output_2
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_2.yml -n
#EXIT_CODE_2=$?
#
#if [[ $EXIT_CODE_2 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_2/matches.json
#ls -lah $PWD/functional_tests/output_2/summary.json
#
## ----------------------------------------------------------------------------------------------------------------------
## Simulate a job with the ids in a text file
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_3 || true
#mkdir -p $PWD/functional_tests/output_3
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_3.yml -n
#EXIT_CODE_3=$?
#
#if [[ $EXIT_CODE_3 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_3/matches.json
#ls -lah $PWD/functional_tests/output_3/summary.json
#
## ----------------------------------------------------------------------------------------------------------------------
## Simulate a job with the ids in a file compressed with the gz format
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_4 || true
#mkdir -p $PWD/functional_tests/output_4
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_4.yml -n
#EXIT_CODE_4=$?
#
#if [[ $EXIT_CODE_4 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_4/matches.json
#ls -lah $PWD/functional_tests/output_4/summary.json
#
## ----------------------------------------------------------------------------------------------------------------------
## Simulate a job with the ids in a file compressed with the zip format
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_5 || true
#mkdir -p $PWD/functional_tests/output_5
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_5.yml -n
#EXIT_CODE_5=$?
#
#if [[ $EXIT_CODE_5 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_5/matches.json
#ls -lah $PWD/functional_tests/output_5/summary.json
#
#
## ----------------------------------------------------------------------------------------------------------------------
## Simulate a job with no ids in the parameters or a file to read them from. The job should fail
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_6 || true
#mkdir -p $PWD/functional_tests/output_6
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_6.yml -n
#EXIT_CODE_6=$?
#
#if [[ $EXIT_CODE_6 -eq 0 ]]; then
#    echo "The job must have failed!"
#    exit 1
#else
#    echo "The job failed. As expected."
#
#fi
#
## ----------------------------------------------------------------------------------------------------------------------
## Simulate a job with the ids in a text file separated by new lines
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_7 || true
#mkdir -p $PWD/functional_tests/output_7
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_7.yml -n
#EXIT_CODE_7=$?
#
#if [[ $EXIT_CODE_7 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_7/matches.json
#ls -lah $PWD/functional_tests/output_7/summary.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate a job with the ids in a CSV file generated from Excel
# ----------------------------------------------------------------------------------------------------------------------
rm -rf $PWD/functional_tests/output_8 || true
mkdir -p $PWD/functional_tests/output_8

PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_8.yml -n
EXIT_CODE_8=$?

if [[ $EXIT_CODE_8 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_8/matches.json
ls -lah $PWD/functional_tests/output_8/summary.json


# ----------------------------------------------------------------------------------------------------------------------
# Simulate a job with the ids in the parameters with automatic separator identification
# ----------------------------------------------------------------------------------------------------------------------
rm -rf $PWD/functional_tests/output_9 || true
mkdir -p $PWD/functional_tests/output_9

PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_9.yml -n
EXIT_CODE_9=$?

if [[ $EXIT_CODE_9 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_9/matches.json
ls -lah $PWD/functional_tests/output_9/summary.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate a job with the ids in a text file
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_10 || true
mkdir -p $PWD/functional_tests/output_10
PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_10.yml -n
EXIT_CODE_10=$?

if [[ $EXIT_CODE_10 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_10/matches.json
ls -lah $PWD/functional_tests/output_10/summary.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate a job with the ids in a text file compressed in gz format
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_11 || true
mkdir -p $PWD/functional_tests/output_11
PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_11.yml -n
EXIT_CODE_11=$?

if [[ $EXIT_CODE_11 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_11/matches.json
ls -lah $PWD/functional_tests/output_11/summary.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate a job with the ids in a text file compressed in gz format
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_12 || true
mkdir -p $PWD/functional_tests/output_12
PYTHONPATH=$PWD:$PYTHONPATH $PWD/search_by_ids_job/run_job.py $PWD/functional_tests/params/run_params_example_12.yml -n
EXIT_CODE_12=$?

if [[ $EXIT_CODE_12 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_12/matches.json
ls -lah $PWD/functional_tests/output_12/summary.json