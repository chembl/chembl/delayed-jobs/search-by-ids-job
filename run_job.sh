#!/usr/bin/env bash

set -x
set -e

source /app/python_env/bin/activate
PYTHONPATH=/app/python_env/lib/python3.8/site-packages/:/app python3 /app/search_by_ids_job/run_job.py $1
deactivate