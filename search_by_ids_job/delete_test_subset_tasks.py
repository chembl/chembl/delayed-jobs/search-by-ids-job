#!/usr/bin/env python3
"""
Script that requests the deletion of test tasks from the ES Subset generator. The server details must be set up by
environment variables
"""
import requests
from requests.auth import HTTPBasicAuth

from search_by_ids_job import utils


class TestTaskDeletionError(Exception):
    """
    Base class for errors in this module
    """


def run():
    """
    Runs the script
    """
    print('GOING TO REQUEST THE DELETION OF TEST TASKS!')

    subset_generator_base_path = utils.get_es_subset_generator_base_url()
    subset_generator_username = utils.get_es_subset_generator_admin_username()
    subset_generator_password = utils.get_es_subset_generator_admin_password()

    admin_login_url = f'{subset_generator_base_path}/admin/login'
    print('admin_login_url: ', admin_login_url)
    login_request = requests.get(admin_login_url,
                                 auth=HTTPBasicAuth(subset_generator_username, subset_generator_password))

    if login_request.status_code != 200:
        raise TestTaskDeletionError(f'There was a problem when logging into the administration of the system! '
                                    f'(Status code: {login_request.status_code})')

    login_response = login_request.json()
    print('Token obtained')

    admin_token = login_response.get('token')
    headers = {'X-Admin-Key': admin_token}

    test_task_deletion_url = f'{subset_generator_base_path}/admin/delete_test_tasks_and_indexes'
    task_deletion_request = requests.get(test_task_deletion_url, headers=headers)
    if task_deletion_request.status_code != 200:
        raise TestTaskDeletionError(f'There was a problem when requesting the deletion of test jobs! '
                                    f'(Status code: {task_deletion_request.status_code})')
    task_deletion_response = task_deletion_request.json()

    print('task_deletion_response: ', task_deletion_response)


if __name__ == "__main__":
    run()
