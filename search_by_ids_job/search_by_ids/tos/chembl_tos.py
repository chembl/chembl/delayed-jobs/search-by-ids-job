"""
Possible to values from ChEMBL
"""
from enum import Enum
from search_by_ids_job.search_by_ids import standardisation


class ChEMBLTos(Enum):
    """
    Class that enumerates the possible tos values
    """
    CHEMBL_COMPOUNDS = 'CHEMBL_COMPOUNDS'
    CHEMBL_TARGETS = 'CHEMBL_TARGETS'
    CHEMBL_ASSAYS = 'CHEMBL_ASSAYS'
    CHEMBL_DOCUMENTS = 'CHEMBL_DOCUMENTS'
    CHEMBL_CELL_LINES = 'CHEMBL_CELL_LINES'
    CHEMBL_TISSUES = 'CHEMBL_TISSUES'

    def get_correspondig_entity_name(self):
        """
        :return: the entity name that corresponds to the To option.
        """
        to_to_entity_name = {
            self.CHEMBL_COMPOUNDS: standardisation.StandardEntityName.COMPOUND.to_string(),
            self.CHEMBL_TARGETS: standardisation.StandardEntityName.TARGET.to_string(),
            self.CHEMBL_ASSAYS: standardisation.StandardEntityName.ASSAY.to_string(),
            self.CHEMBL_DOCUMENTS: standardisation.StandardEntityName.DOCUMENT.to_string(),
            self.CHEMBL_CELL_LINES: standardisation.StandardEntityName.CELL_LINE.to_string(),
            self.CHEMBL_TISSUES: standardisation.StandardEntityName.TISSUE.to_string(),
        }

        entity_name = to_to_entity_name[self]
        return entity_name
