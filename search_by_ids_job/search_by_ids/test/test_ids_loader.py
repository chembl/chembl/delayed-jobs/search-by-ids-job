"""
Module to test the ids loader
"""
import unittest
import base64
import gzip
import sys

from search_by_ids_job.search_by_ids import ids_loader, standardisation


class IDsLoaderTest(unittest.TestCase):
    """
    Class to test the ids loader module
    """

    def test_ids_standardisation(self):
        """
        Test that the ids are standardised regardless of the separator
        """

        items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
        items_ids.sort()

        for original_separator in [',', ';', '\n', '\t']:
            raw_item_ids = original_separator.join(items_ids)

            ids_manager = ids_loader.IDsLoader({'raw_items_ids': raw_item_ids, 'separator': original_separator})
            standardised_item_ids_got = ids_manager.standardised_raw_ids
            parsed_item_ids_got = standardised_item_ids_got.split(standardisation.STANDARD_SEPARATOR)
            parsed_item_ids_got.sort()

            self.assertEqual(items_ids, parsed_item_ids_got,
                             msg=f'Items separated with {original_separator} were not standardised correctly!!!')

    def test_ids_uniqueness(self):
        """
        Test that the ids are unique, duplicate ids are eliminated
        """

        items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667', 'CHEMBL4303667',
                     'CHEMBL4303667', 'CHEMBL4303667']
        unique_items_ids = set(items_ids)
        items_ids_must_be = list(unique_items_ids)
        items_ids_must_be.sort()

        for original_separator in [',', ';', '\n', '\t']:
            raw_item_ids = original_separator.join(items_ids)

            ids_manager = ids_loader.IDsLoader({'raw_items_ids': raw_item_ids, 'separator': original_separator})

            standardised_item_ids_got = ids_manager.standardised_raw_ids
            parsed_item_ids_got = standardised_item_ids_got.split(standardisation.STANDARD_SEPARATOR)
            parsed_item_ids_got.sort()

            self.assertEqual(items_ids_must_be, parsed_item_ids_got,
                             msg=f'Duplicate items separated with {original_separator} '
                                 f'were not eliminated correctly!!!')

    def test_ids_uniqueness_statistics(self):
        """
        Test that the statistics of the uniqueness of the ids are calculated correctly
        """

        items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667', 'CHEMBL4303667',
                     'CHEMBL4303667', 'CHEMBL4303667']

        total_ids_entered_must_be = len(items_ids)
        unique_items_ids = set(items_ids)
        items_ids_must_be = list(unique_items_ids)
        items_ids_must_be.sort()
        total_unique_ids_must_be = len(unique_items_ids)
        duplicate_ids_found_must_be = total_ids_entered_must_be - total_unique_ids_must_be

        for original_separator in [',', ';', '\n', '\t']:
            raw_item_ids = original_separator.join(items_ids)

            ids_manager = ids_loader.IDsLoader({'raw_items_ids': raw_item_ids, 'separator': original_separator})

            total_ids_entered_must_got = ids_manager.stats['total_ids_entered']
            self.assertEqual(total_ids_entered_must_got, total_ids_entered_must_be,
                             msg=f'The total of ids entered with separator {original_separator} '
                                 f'was not calculated correctly!!!')

            total_unique_ids_got = ids_manager.stats['total_unique_ids']
            self.assertEqual(total_unique_ids_got, total_unique_ids_must_be,
                             msg=f'The total of unique ids entered with separator {original_separator} '
                                 f'was not calculated correctly!!!')

            duplicate_ids_found_got = ids_manager.stats['duplicate_ids_found']
            self.assertEqual(duplicate_ids_found_got, duplicate_ids_found_must_be,
                             msg=f'The total of duplicate ids found with separator {original_separator} '
                                 f'was not calculated correctly!!!')

    def test_compresses_ids(self):
        """
        Tests that it compresses the ids entered and encodes them in base 64 as required by the subset generator service
        """

        items_ids_must_be = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
        items_ids_must_be.sort()

        for original_separator in [',', ';', '\n', '\t']:
            raw_item_ids = original_separator.join(items_ids_must_be)

            ids_manager = ids_loader.IDsLoader({'raw_items_ids': raw_item_ids, 'separator': original_separator})
            compressed_ids_got = ids_manager.compressed_ids

            base64_bytes = base64.b64decode(compressed_ids_got)
            decompressed_bytes = gzip.decompress(base64_bytes)
            raw_ids = decompressed_bytes.decode()
            items_ids_got = raw_ids.split(',')
            items_ids_got.sort()

            self.assertEqual(items_ids_got, items_ids_must_be,
                             msg=f'Items separated with {original_separator} were not compressed correctly!!!')

    def test_calculates_compressed_and_expanded_size(self):
        """
        Tests that it calculates the compressed and expanded size for the statistics
        """
        items_ids_must_be = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
        items_ids_must_be.sort()

        for original_separator in [',', ';', '\n', '\t']:
            raw_item_ids = original_separator.join(items_ids_must_be)

            ids_manager = ids_loader.IDsLoader({'raw_items_ids': raw_item_ids, 'separator': original_separator})
            standardised_raw_ids = ids_manager.standardised_raw_ids
            num_uncompressed_bytes_must_be = sys.getsizeof(standardised_raw_ids)
            num_uncompressed_bytes_got = ids_manager.stats['num_uncompressed_bytes']

            self.assertEqual(num_uncompressed_bytes_must_be, num_uncompressed_bytes_got,
                             msg='The size of the uncompressed ids was not calculated correctly!')

            compressed_ids_got = ids_manager.compressed_ids
            num_compressed_bytes_must_be = sys.getsizeof(compressed_ids_got)
            num_compressed_bytes_got = ids_manager.stats['num_compressed_bytes']

            self.assertEqual(num_compressed_bytes_must_be, num_compressed_bytes_got,
                             msg='The size of the compressed ids was not calculated correctly!')

            compression_ratio_must_be = num_compressed_bytes_must_be / num_uncompressed_bytes_must_be
            compression_ratio_got = ids_manager.stats['compression_ratio']

            self.assertEqual(compression_ratio_must_be, compression_ratio_got,
                             msg='The compression ratio was not calculated correctly!')

    def test_sets_ids_not_in_subset(self):
        """
        Tests it can receive ids that should not be included in the subset. The compressed ids should not include the
        ids that must not be included in the generated subset
        """
        all_initial_ids = {'CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667'}
        ids_not_in_subset = {'CHEMBL27193', 'CHEMBL4068896'}

        items_ids_must_be = list(all_initial_ids - ids_not_in_subset)

        items_ids_must_be.sort()

        for original_separator in [',', ';', '\n', '\t']:
            raw_item_ids = original_separator.join(all_initial_ids)

            ids_manager = ids_loader.IDsLoader({'raw_items_ids': raw_item_ids, 'separator': original_separator})
            ids_manager.update_ids_not_in_subset(ids_not_in_subset)

            compressed_ids_got = ids_manager.compressed_ids

            base64_bytes = base64.b64decode(compressed_ids_got)
            decompressed_bytes = gzip.decompress(base64_bytes)
            raw_ids = decompressed_bytes.decode()
            items_ids_got = raw_ids.split(',')
            items_ids_got.sort()

            self.assertEqual(items_ids_got, items_ids_must_be,
                             msg=f'Items separated with {original_separator} were not compressed correctly!!!')
