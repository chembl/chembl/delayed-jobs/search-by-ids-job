"""
Module that handles the connection with elasticsearch
"""
import os

import elasticsearch


class ElasticsearchConnectionConfigError(Exception):
    """
    Class for errors in the ES configuration
    """


def get_es_connection_from_env_config():
    """
    The following env variables need to be set up:
    ELASTICSEARCH_HOST
    ELASTICSEARCH_PORT
    ELASTICSEARCH_USERNAME
    ELASTICSEARCH_PASSWORD
    :return: a new ES connection with the configuration set in the parameters.
    """
    elasticsearch_host = os.environ.get('ELASTICSEARCH_HOST')
    print('elasticsearch_host: ', elasticsearch_host)
    elasticsearch_port = os.environ.get('ELASTICSEARCH_PORT')
    print('elasticsearch_port: ', elasticsearch_port)
    elasticsearch_username = os.environ.get('ELASTICSEARCH_USERNAME')
    print('elasticsearch_username: ', elasticsearch_username)
    elasticsearch_password = os.environ.get('ELASTICSEARCH_PASSWORD')
    print('elasticsearch_password: ', elasticsearch_password)

    for param in [elasticsearch_host, elasticsearch_port, elasticsearch_username, elasticsearch_password]:

        if param is None:
            raise ElasticsearchConnectionConfigError(f'The connection to ES is not properly configured. '
                                                     f'You need to set up the environment variables correctly')

    return get_es_connection(elasticsearch_host, elasticsearch_port, elasticsearch_username, elasticsearch_password)


def get_es_connection(elasticsearch_host, elasticsearch_port, username, password):
    """
    :param elasticsearch_host: host of elasticsearch to connect to
    :param elasticsearch_port: port of elasticsearch
    :param username: username for elasticsearch
    :param password: password for elasticsearch
    :return: a new connection with the parameter given
    """

    return elasticsearch.Elasticsearch(
        hosts=[{
            'host': elasticsearch_host,
            'port': elasticsearch_port,
            'transport_class': elasticsearch.Urllib3HttpConnection,
            'timeout': 60
        }],
        http_auth=(username, password),
        retry_on_timeout=True
    )
