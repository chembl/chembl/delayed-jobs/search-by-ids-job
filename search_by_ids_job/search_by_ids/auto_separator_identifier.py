"""
Module with functions to help in the automatic identification of separators
"""
import requests

from search_by_ids_job import utils

SAMPLE_MAX_SIZE = 200


def get_separator(type_of_ids, raw_ids):
    """
    Uses the ES Proxy Utils endpoint to get the separator used
    :param type_of_ids: type of ids entered
    :param raw_ids: text with the ids entered
    :return: string with the characters that separate the items
    """
    print('Going to identify the separator')
    es_proxy_base_url = utils.get_es_proxy_base_url()
    separator_identifier_url = f'{es_proxy_base_url}/utils/identify_separator'
    print('separator_identifier_url: ', separator_identifier_url)

    sample = raw_ids
    if len(sample) > SAMPLE_MAX_SIZE:
        sample = sample[:SAMPLE_MAX_SIZE]

    payload = {
        'type_of_ids': type_of_ids,
        'text': sample
    }

    print('payload: ', payload)

    request = requests.post(separator_identifier_url, data=payload)

    status_code = request.status_code
    print(f'status_code: {status_code}')

    response_json = request.json()
    print('response_json: ', response_json)
    separator = response_json['separator']
    print('separator: ', separator)

    return separator
