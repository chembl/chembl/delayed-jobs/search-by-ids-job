"""
Possible from values from ChEMBL
"""
from enum import Enum


class ChEMBLFroms(Enum):
    """
    Class that enumerates the possible froms values that are for ids owned by ChEMBL
    """
    MOLECULE_CHEMBL_IDS = 'MOLECULE_CHEMBL_IDS'
    TARGET_CHEMBL_IDS = 'TARGET_CHEMBL_IDS'
    ASSAY_CHEMBL_IDS = 'ASSAY_CHEMBL_IDS'
    DOCUMENT_CHEMBL_IDS = 'DOCUMENT_CHEMBL_IDS'
    CELL_LINE_CHEMBL_IDS = 'CELL_LINE_CHEMBL_IDS'
    TISSUE_CHEMBL_IDS = 'TISSUE_CHEMBL_IDS'

    def __str__(self):
        return self.name
