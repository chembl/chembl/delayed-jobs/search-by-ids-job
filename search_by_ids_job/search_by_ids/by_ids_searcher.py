"""
Package that implements the search by IDs job
"""
import json
from pathlib import Path

from search_by_ids_job.search_by_ids import subset_creation_requests
from search_by_ids_job.search_by_ids.ids_mapping import ids_mapper
from search_by_ids_job.search_by_ids import job_progress


class ByIDsSearcherError(Exception):
    """
    Base class for errors in this module
    """


def do_search(job_params, input_files, summary_output_path, server_connection):
    """
    Performs a search by ids from the ids passed as parameter
    :param job_params: params for the job
    :param input_files: dictionary input files
    :param summary_output_path: path where to save the summary output
    :param server_connection: connection to the delayed jobs server
    :return: the statistics object to save in elasticsearch
    """

    raw_from = job_params['from']
    raw_to = job_params['to']
    raw_items_ids = job_params.get('raw_items_ids')
    separator = job_params['separator']

    # Summary to show to the user
    summary = {
        "entity_from": raw_from,
        "entity_to": raw_to,
        "separator": separator,
    }
    # statistics to send to the statistics
    stats = {}

    # dict describing the input
    input_description = {
        'raw_items_ids': raw_items_ids,
        'input_files': input_files,
        'separator': separator,
        'raw_from': raw_from,
        'raw_to': raw_to
    }

    job_progress.initialise_server_connection(server_connection)
    the_ids_mapper = ids_mapper.IDsMapper(input_description, summary, stats)

    subset_creation_request = handle_search_and_submit_subset_task(the_ids_mapper)
    subset_creation_request.launch_and_wait_for_subset_creation(summary)

    job_progress.report_step_reached(step_number=5, in_progress=True)

    write_summary(summary_output_path, summary)

    matches = subset_creation_request.matches
    write_matches(matches, summary_output_path)

    server_connection.update_job_progress(100, status_log_msg='Search finished successfully!')
    job_progress.report_step_reached(step_number=5, in_progress=False)

    # statistics to be sent to the delayed jobs system
    full_stats = {
        **summary,
        **stats,
        **job_progress.get_step_times_dict()
    }
    return full_stats


def handle_search_and_submit_subset_task(the_ids_mapper):
    """
    Receives the ids mapper, and triggers the creation of the index task
    :param the_ids_mapper: configuration of the from to case
    :return: instance of SubsetCreationRequest and the matches dict obtained after the search
    """

    origin_index = the_ids_mapper.input_description['from_to_config']['origin_index']
    the_ids_mapper.do_mapping_to_chembl_ids()
    compressed_items_ids = the_ids_mapper.get_destination_ids_compressed()
    matches = the_ids_mapper.mapping_dict

    subset_creation_request = subset_creation_requests.SubsetCreationRequest(origin_index, compressed_items_ids,
                                                                             matches)

    return subset_creation_request


def write_summary(summary_output_path, task_summary):
    """
    Writes the summary to the path indicated as parameter
    :param summary_output_path: path where to save summary
    :param task_summary: summary of the task performed
    """

    print('----------------------------')
    print(f'going to write summary of search to: {summary_output_path}')
    print('----------------------------')

    summary_file_path = Path.joinpath(Path(summary_output_path), 'summary.json')
    write_json_file(summary_file_path, task_summary)


def write_matches(matches, summary_output_path):
    """
    Writes the matches dict in json and csv formats
    :param matches: matches dict
    :param summary_output_path: output directory path for the job
    """

    write_matches_json(matches, summary_output_path)
    write_matches_csv(matches, summary_output_path)


def write_matches_csv(matches, summary_output_path):
    """
    :param matches: matches dict
    :param summary_output_path: output directory path for the job
    """
    headers = ['Input Term', 'Match', 'Entity Type', 'ID Status', 'In Results', 'Comments']

    rows = []
    for input_term, matches_description in matches.items():

        if len(matches_description['matches']) == 0:
            new_row = [input_term, '', '', '', '', '']
            rows.append(new_row)
            continue

        for matched_chembl_id, match_desc in matches_description['matches'].items():
            entity_type = match_desc['entity_type']
            id_status = match_desc['id_status']
            in_results = str(match_desc['in_results'])
            comments = ' | '.join(match_desc['comments'])

            new_row = [input_term, matched_chembl_id, entity_type, id_status, in_results, comments]
            rows.append(new_row)
            continue

    csv_file_path = Path.joinpath(Path(summary_output_path), 'matches.csv')
    write_csv_file(csv_file_path, headers, rows)


def write_matches_json(matches, summary_output_path):
    """
    :param matches: matches dict
    :param summary_output_path: output directory path for the job
    """

    matches_file_path = Path.joinpath(Path(summary_output_path), 'matches.json')
    write_json_file(matches_file_path, matches)


# ----------------------------------------------------------------------------------------------------------------------
# Utils
# ----------------------------------------------------------------------------------------------------------------------
def write_json_file(file_path, dict_to_save):
    """
    Writes the dict passed as parameter to the path passed as parameter
    :param file_path: path were you want to save the file
    :param dict_to_save: dict object to save
    """
    with open(file_path, 'wt') as out_file:
        json.dump(dict_to_save, out_file)


def write_csv_file(file_path, headers, rows):
    """
    Writes the headers and the rows passed as parameter to the path passed as parameter
    :param file_path: path were you want to save the file
    :param headers: list of headers of the table
    :param rows: list of rows of the table
    """
    with open(file_path, 'wt') as out_file:
        parsed_headers = [f'"{header}"' for header in headers]
        out_file.write(','.join(parsed_headers) + '\n')

        for row in rows:
            parsed_row = [f'"{cell}"' for cell in row]
            out_file.write(','.join(parsed_row) + '\n')
