"""
Module that implements functions to do the ids mapping
"""
from search_by_ids_job.search_by_ids import ids_loader
from search_by_ids_job.search_by_ids import chembl_id_lookup
from search_by_ids_job.search_by_ids.ids_mapping import from_chembl_to_chembl
from search_by_ids_job.search_by_ids import job_progress
from search_by_ids_job.search_by_ids.froms.chembl_froms import ChEMBLFroms
from search_by_ids_job.search_by_ids.tos.chembl_tos import ChEMBLTos


class IDsMapperError(Exception):
    """
    Base class for errors in this module
    """


FROM_TO_CONFIG = {
    ChEMBLFroms.MOLECULE_CHEMBL_IDS: {
        ChEMBLTos.CHEMBL_COMPOUNDS: {
            'origin_index': 'chembl_molecule'
        }
    },
    ChEMBLFroms.TARGET_CHEMBL_IDS: {
        ChEMBLTos.CHEMBL_TARGETS: {
            'origin_index': 'chembl_target'
        }
    },
    ChEMBLFroms.ASSAY_CHEMBL_IDS: {
        ChEMBLTos.CHEMBL_ASSAYS: {
            'origin_index': 'chembl_assay'
        }
    },
    ChEMBLFroms.DOCUMENT_CHEMBL_IDS: {
        ChEMBLTos.CHEMBL_DOCUMENTS: {
            'origin_index': 'chembl_document'
        }
    },
    ChEMBLFroms.CELL_LINE_CHEMBL_IDS: {
        ChEMBLTos.CHEMBL_CELL_LINES: {
            'origin_index': 'chembl_cell_line'
        }
    },
    ChEMBLFroms.TISSUE_CHEMBL_IDS: {
        ChEMBLTos.CHEMBL_TISSUES: {
            'origin_index': 'chembl_tissue'
        }
    }
}


def is_from_chembl_to_chembl(entity_from):
    """
    :param entity_from: entity from as passed in the job parameters
    :return: True if the search is from chembl to chembl, False otherwise
    """
    return entity_from in ChEMBLFroms


def parse_from_and_to_parameters(raw_from, raw_to):
    """
    Parses the strings with the from parameters and to parameters to the enumerations defined in this module
    :param raw_from: string with the from parameter
    :param raw_to: string with the to parameter
    :return: a tuple with the parsed from and to
    """
    try:
        entity_from = ChEMBLFroms(raw_from)
    except ValueError:
        raise IDsMapperError(
            f'{raw_from} is not a valid from! Valid ones are {[valid_from.value for valid_from in ChEMBLFroms]}')

    try:
        entity_to = ChEMBLTos(raw_to)
    except ValueError:
        raise IDsMapperError(
            f'{raw_to} is not a valid to! Valid ones are {[valid_to.value for valid_to in ChEMBLTos]}')

    try:
        from_to_config = FROM_TO_CONFIG[entity_from][entity_to]
    except KeyError:
        msg = ''
        for possible_from_key, possible_tos in FROM_TO_CONFIG.items():
            msg += f'From {possible_from_key.value} to:\n'
            for possible_to_key in possible_tos:
                msg += f'  - {possible_to_key.value}\n'
            msg += '\n'

        raise IDsMapperError(msg)

    return entity_from, entity_to, from_to_config


# ----------------------------------------------------------------------------------------------------------------------
# IDs Mapper class
# ----------------------------------------------------------------------------------------------------------------------
class IDsMapper:
    """
    Class that maps the ids form the source entity to the destination entity
    """

    def __init__(self, input_description, summary=None, stats=None):

        self.input_description = input_description

        if summary is None:
            self.summary = {}
        else:
            self.summary = summary

        if stats is None:
            self.stats = {}
        else:
            self.stats = stats

        entity_from, entity_to, from_to_config = parse_from_and_to_parameters(
            self.input_description['raw_from'], self.input_description['raw_to']
        )

        self.input_description['entity_from'] = entity_from
        self.input_description['entity_to'] = entity_to
        self.input_description['from_to_config'] = from_to_config

        chembl_to_chembl = is_from_chembl_to_chembl(entity_from)
        self.input_description['chembl_to_chembl'] = chembl_to_chembl

        # IDs entered by the use, they can be chembl ids, Uniprot, Pubchem, or from any other source supported
        self.source_ids_manager = ids_loader.IDsLoader(self.input_description)
        self.source_ids_manager.append_loading_stats_to_summary(self.summary)
        # ChEMBL IDs mapped from the ids entered
        self.chembl_ids_manager = None
        # Results of the ChEMBL ID lookup
        self.lookup_results = None
        self.mapping_dict = None

    def do_mapping_to_chembl_ids(self):
        """
        Takes the source ids loader and uses it to get chembl ids
        """
        self.generate_initial_ids_mapping()
        chembl_to_chembl = self.input_description['chembl_to_chembl']

        # other options will be implemented later
        if chembl_to_chembl:
            self.chembl_ids_manager = self.source_ids_manager
            self.do_id_lookup()
            matches_dict = self.mapping_dict
            lookup_results = self.lookup_results
            desired_std_type = self.input_description['entity_to'].get_correspondig_entity_name()
            from_chembl_to_chembl.process_ids_lookup(lookup_results, matches_dict, desired_std_type)
            self.calculate_ids_not_in_subset()

        self.chembl_ids_manager.append_compression_stats_to_stats(self.stats)

    def generate_initial_ids_mapping(self):
        """
        Generates the initial ids mapping dict. To be filled after the chembl id lookup is run
        """
        job_progress.report_step_reached(step_number=1, in_progress=True)
        # in this case the mapping dict is quite straight forward, some fields are given default values.
        mapping_dict = {}
        for mapped_id in self.source_ids_manager.ids:
            match = {
                'input_term': mapped_id,
                'matches': {}
            }
            mapping_dict[mapped_id] = match

        self.mapping_dict = mapping_dict
        job_progress.report_step_reached(step_number=1, in_progress=False)

    def do_id_lookup(self):
        """
        Does the ID lookup for the for the chembl ids obtained.
        """
        job_progress.report_step_reached(step_number=2, in_progress=True)
        ids_list = list(self.chembl_ids_manager.ids)
        self.lookup_results = chembl_id_lookup.get_lookup_results(ids_list)
        job_progress.report_step_reached(step_number=2, in_progress=False)

    def calculate_ids_not_in_subset(self):
        """
        Calculates the ids NOT to be sent to the subset generator. Based on the mapping obtained after the lookup.
        Sets the excluded ids on the chembl_ids_manager so they are not used for the subset.
        """
        job_progress.report_step_reached(step_number=3, in_progress=True)
        ids_not_in_subset = set()
        candidate_chembl_ids = self.chembl_ids_manager.ids

        num_items_with_match = 0
        num_items_with_no_match = 0
        num_ids_included_in_results = 0
        num_inactive_items = 0
        num_obsolete_items = 0
        num_other_excluded_items = 0
        for input_term, matches_description in self.mapping_dict.items():
            # count if it has matches or not
            if len(matches_description.get('matches', {})) == 0:
                num_items_with_no_match += 1
            else:
                num_items_with_match += 1

            # remove the ids that do not exist for the case of from chembl to chembl
            if input_term in candidate_chembl_ids:
                if len(matches_description.get('matches', {})) == 0:
                    ids_not_in_subset.add(input_term)

            # remove the ids that are not in results
            for matched_chembl_id, match_desc in matches_description['matches'].items():
                if not match_desc['in_results']:
                    ids_not_in_subset.add(matched_chembl_id)
                    if match_desc['id_status'] == 'Inactive':
                        num_inactive_items += 1
                    elif match_desc['id_status'] == 'Obsolete':
                        num_obsolete_items += 1
                    else:
                        num_other_excluded_items += 1
                else:
                    num_ids_included_in_results += 1

        self.chembl_ids_manager.update_ids_not_in_subset(ids_not_in_subset)
        self.summary['num_items_with_match'] = num_items_with_match
        self.summary['num_items_with_no_match'] = num_items_with_no_match
        self.summary['num_ids_included_in_results'] = num_ids_included_in_results
        self.summary['num_inactive_items'] = num_inactive_items
        self.summary['num_obsolete_items'] = num_obsolete_items
        self.summary['num_other_excluded_items'] = num_other_excluded_items

        job_progress.report_step_reached(step_number=3, in_progress=False)

    def get_destination_chembl_ids_list(self):
        """
        :return: the chembl ids obtained after the mapping
        """
        return list(self.chembl_ids_manager.ids)

    def get_destination_ids_compressed(self):
        """
        :return: the chembl ids obtained after the mapping compressed and encoded in base64
        """
        return self.chembl_ids_manager.compressed_ids
