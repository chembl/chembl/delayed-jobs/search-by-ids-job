"""
Module that hanldes the mapping of ids from chembl to chembl
"""


def process_ids_lookup(ids_lookup_result, matches_dict, desired_std_type):
    """
    Given the ids lookup response and the matches dict it fills the matches dict with the information
    known from the lookup
    :param ids_lookup_result: full dictionary from the ids lookup
    :param desired_std_type: Standard type desired for the search, to discard the ids that belong to a different type
    than the desired one
    :param matches_dict: dict describing the matches. Will be modified by this function
    """
    for matched_chembl_id, lookup_result in ids_lookup_result.items():

        std_type = lookup_result['std_type']
        std_status = lookup_result['status']
        type_is_the_same_as_desired = std_type == desired_std_type
        in_results = type_is_the_same_as_desired and std_status == 'Active'
        comments = []

        if not in_results:

            if not type_is_the_same_as_desired:
                comments.append(
                    f'The type of the matched id, {std_type}, is different from the desired type {desired_std_type}.'
                )

            if std_status == 'Obsolete':
                comments.append(f'The id is obsolete, it has been permanently deleted from ChEMBL.')

            if std_status == 'Inactive':
                comments.append(f'The id is inactive, it has not been included in the latest release.')

        match_description = {
            'entity_type': std_type,
            'id_status': std_status,
            'in_results': in_results,
            'comments': comments
        }

        matches_dict[matched_chembl_id]['matches'][matched_chembl_id] = match_description
