"""
Module to test the mapping of ids from chembl to chembl
"""
# pylint: disable=too-many-locals
import unittest
import json

from search_by_ids_job.search_by_ids.ids_mapping import from_chembl_to_chembl
from search_by_ids_job.search_by_ids.ids_mapping import ids_mapper


class MappingFromChEMBLToChEMBLTest(unittest.TestCase):
    """
    Class to test the ids mapper module
    """

    def test_processes_id_lookup_from_compound_to_compounds(self):
        """
        Test that it processes the ids lookup when searching from the molecule ChEMBL IDs to ChEMBL Compounds
        """

        test_ids_file_path = 'search_by_ids_job/search_by_ids/ids_mapping/test/data/example1/compound_ids_1.json'
        with open(test_ids_file_path) as test_ids_file:
            items_ids = json.load(test_ids_file)['test_ids']
            test_ids_lookup_response_file_path = \
                'search_by_ids_job/search_by_ids/ids_mapping/test/data/example1/ids_lookup_1.json'

            with open(test_ids_lookup_response_file_path) as test_ids_lookup_response_file:
                test_ids_lookup_response = json.load(test_ids_lookup_response_file)

                original_separator = ','
                raw_from = 'MOLECULE_CHEMBL_IDS'
                raw_to = 'CHEMBL_COMPOUNDS'
                raw_item_ids = original_separator.join(items_ids)
                input_description = {
                    'raw_items_ids': raw_item_ids,
                    'separator': original_separator,
                    'raw_from': raw_from,
                    'raw_to': raw_to
                }
                the_ids_mapper = ids_mapper.IDsMapper(input_description)
                the_ids_mapper.generate_initial_ids_mapping()
                matches_dict = the_ids_mapper.mapping_dict

                desired_std_type = 'Compound'
                from_chembl_to_chembl.process_ids_lookup(test_ids_lookup_response, matches_dict, desired_std_type)

                matches_must_be_path = \
                    'search_by_ids_job/search_by_ids/ids_mapping/test/data/example1/matches_must_be.json'

                with open(matches_must_be_path) as matches_must_be_file:
                    matches_must_be = json.load(matches_must_be_file)
                    print('matches_dict')
                    print(json.dumps(matches_dict))

                    self.assertEqual(matches_dict, matches_must_be, msg='The matches dict was not generated correctly!')
