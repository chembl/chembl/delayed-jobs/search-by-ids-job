"""
Module to test the ids mapper
"""
import unittest

from search_by_ids_job.search_by_ids.ids_mapping import ids_mapper

class IDsMapperTest(unittest.TestCase):
    """
    Class to test the ids mapper module
    """

    # ------------------------------------------------------------------------------------------------------------------
    # Parameter parsing
    # ------------------------------------------------------------------------------------------------------------------
    def test_parses_from_and_to_parameters(self):
        """
        Tests that id parses the from and to parameters
        """
        raw_from = 'MOLECULE_CHEMBL_IDS'
        raw_to = 'CHEMBL_COMPOUNDS'

        parsed_params = ids_mapper.parse_from_and_to_parameters(raw_from, raw_to)
        from_got, to_got = parsed_params[0], parsed_params[1]

        self.assertIs(from_got, ids_mapper.ChEMBLFroms.MOLECULE_CHEMBL_IDS,
                      msg='The from was not parsed successfully!')

        self.assertIs(to_got, ids_mapper.ChEMBLTos.CHEMBL_COMPOUNDS,
                      msg='The to was not parsed successfully!')

    def test_raises_error_when_from_does_not_exist(self):
        """
        Test that it raises an exception when the from does not exist
        """

        raw_from = 'DOES_NOT_EXIST'
        raw_to = 'CHEMBL_COMPOUNDS'

        with self.assertRaises(ids_mapper.IDsMapperError, msg='This should raise a ByIdsSearcherError!'):
            ids_mapper.parse_from_and_to_parameters(raw_from, raw_to)

    def test_raises_error_when_to_does_not_exist(self):
        """
        Test that it raises an exception when the to does not exist
        """

        raw_from = 'MOLECULE_CHEMBL_IDS'
        raw_to = 'DOES_NOT_EXIST'

        with self.assertRaises(ids_mapper.IDsMapperError,
                               msg='This should raise a ByIdsSearcherError!'):
            ids_mapper.parse_from_and_to_parameters(raw_from, raw_to)

    def test_raises_error_from_and_to_do_not_match(self):
        """
        Test it raises an error when the from and to do not match
        """
        raw_from = 'MOLECULE_CHEMBL_IDS'
        raw_to = 'CHEMBL_TARGETS'

        with self.assertRaises(ids_mapper.IDsMapperError,
                               msg='This should raise a ByIdsSearcherError!'):
            ids_mapper.parse_from_and_to_parameters(raw_from, raw_to)

    # ------------------------------------------------------------------------------------------------------------------
    # Is from chembl to chembl
    # ------------------------------------------------------------------------------------------------------------------
    def test_identifies_when_it_is_from_chembl_to_chembl(self):
        """
        Test it identifies when it is from chembl to chembl
        """
        for valid_from in ids_mapper.ChEMBLFroms:
            self.assertTrue(ids_mapper.is_from_chembl_to_chembl(valid_from),
                            msg=f'It should identify from {valid_from.value} as from chembl to chembl!')

    # ------------------------------------------------------------------------------------------------------------------
    # IDs mapping
    # ------------------------------------------------------------------------------------------------------------------
    def test_generates_initial_mapping_dict(self):
        """
        Test that generates the initial mapping dict
        """

        items_ids = ['CHEMBL27193', 'CHEMBL4068896', 'CHEMBL332148', 'CHEMBL2431212', 'CHEMBL4303667']
        items_ids.sort()

        mapping_dict_must_be = {
            'CHEMBL27193': {
                'input_term': 'CHEMBL27193',
                'matches': {}
            },
            'CHEMBL4068896': {
                'input_term': 'CHEMBL4068896',
                'matches': {}
            },
            'CHEMBL332148': {
                'input_term': 'CHEMBL332148',
                'matches': {}
            },
            'CHEMBL2431212': {
                'input_term': 'CHEMBL2431212',
                'matches': {}
            },
            'CHEMBL4303667': {
                'input_term': 'CHEMBL4303667',
                'matches': {}
            }
        }

        for original_separator in [',', ';', '\n', '\t']:
            raw_item_ids = original_separator.join(items_ids)
            raw_from = 'MOLECULE_CHEMBL_IDS'
            raw_to = 'CHEMBL_COMPOUNDS'

            input_description = {
                'raw_items_ids': raw_item_ids,
                'separator': original_separator,
                'raw_from': raw_from,
                'raw_to': raw_to
            }
            the_ids_mapper = ids_mapper.IDsMapper(input_description)
            the_ids_mapper.generate_initial_ids_mapping()

            mapping_dict_got = the_ids_mapper.mapping_dict

            self.assertEqual(mapping_dict_got, mapping_dict_must_be,
                             msg=f'The mapping dict was not obtained correctly (separator: {original_separator})')
