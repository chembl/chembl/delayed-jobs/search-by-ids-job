"""
Module that implements functions to load the ids
"""
# pylint: disable=too-many-instance-attributes
import base64
import gzip
import sys
import re
import zipfile

from search_by_ids_job.search_by_ids import standardisation
from search_by_ids_job.search_by_ids import auto_separator_identifier


class IDsLoaderError(Exception):
    """
    Class for errors in this module
    """


def get_cleaned_up_raw_ids(raw_content, original_separator, ids_type=None):
    """
    Splits and cleans up the ids from the raw string
    :param raw_content: content with the ids in plain text
    :param original_separator: character used to separate the ids
    :param ids_type: type of the IDs entered
    :return: a list with the ids cleaned up
    """
    separator = original_separator
    if separator == standardisation.AUTO_SEPARATOR_NAME:
        # separator defaults to , if it can not be identified.
        separator = auto_separator_identifier.get_separator(ids_type, raw_content) or ','

    if separator in ['\n', standardisation.NEW_LINE_NAME]:
        raw_ids_list = raw_content.splitlines()
    else:
        raw_ids_list = raw_content.split(separator)

    raw_ids_list = [raw_id.strip().replace("\n", "").replace("\r", "").replace('\u200c', '').replace('\ufeff', '') for
                    raw_id in raw_ids_list]
    print('------------------------')
    print(f'Read {len(raw_ids_list)} ids from input')
    print('------------------------')
    return raw_ids_list


class IDsLoader:
    """
    Class that helps in managing the ids passed to the job
    """

    NEW_LINE_NAME = '__NEW_LINE__'

    def __init__(self, input_description):

        raw_ids = input_description.get('raw_items_ids')
        self.ids_type = str(input_description.get('entity_from'))
        self.original_separator = input_description['separator']
        if self.original_separator == standardisation.NEW_LINE_NAME:
            self.original_separator = '\n'

        self.ids_are_in_file = False
        if input_description.get('input_files') is not None:
            if input_description.get('input_files').get('input1'):
                self.ids_are_in_file = True

        if raw_ids is None and not self.ids_are_in_file:
            raise IDsLoaderError('You must provide IDs in either the job params or an input file. '
                                 'None of them were provided!')

        self.stats = {
            'num_uncompressed_bytes': None,
            'num_compressed_bytes': None,
            'compression_ratio': None,
            'total_ids_entered': None,
            'total_unique_ids': None,
            'duplicate_ids_found': None,
            'num_ids_not_in_set': None
        }

        self.ids = {}

        if self.ids_are_in_file:
            self.load_ids_from_file(input_description.get('input_files'))
        else:
            self.load_ids_from_raw_list(raw_ids)

        self.standardised_raw_ids = None

        self.ids_not_in_set = set()
        self.standardise_ids()

        self.compressed_ids = None
        self.compress_ids()

    def load_ids_from_raw_list(self, raw_ids):
        """
        Loads the ids from the raw string separated by the separator indicated
        :param raw_ids: string with the ids to parse
        """

        raw_ids_list = get_cleaned_up_raw_ids(raw_ids, self.original_separator, self.ids_type)
        self.stats['total_ids_entered'] = len(raw_ids_list)
        self.ids = set(raw_ids_list)

    def load_ids_from_file(self, input_files_description):
        """
        Loads the ids from the file description provided
        :param input_files_description: dictionary describing the input files for the job
        """
        input_file_path = input_files_description['input1']

        file_extension_matches = re.search(r'\.[^\.]*$', input_file_path)

        if file_extension_matches is None:
            file_name = re.search(r'/[^/]*$', input_file_path)
            raise IDsLoaderError(f'Cannot identify the file type of the file {file_name}')

        file_extension = file_extension_matches.group(0)
        if file_extension.lower() in ['.gz']:
            print(f'Assuming {input_file_path} is a GZIP file')
            self.load_ids_from_gz_file(input_file_path)
        elif file_extension.lower() in ['.zip']:
            print(f'Assuming {input_file_path} is a ZIP file')
            self.load_ids_from_zip_file(input_file_path)
        else:
            print(f'Assuming {input_file_path} is a clear text file')
            self.load_ids_from_raw_txt_file(input_file_path)

    def load_ids_from_raw_txt_file(self, input_file_path):
        """
        loads the ids from a text file
        :param input_file_path: path where to find the file
        """
        with open(input_file_path, 'rt') as input_file:
            raw_content = input_file.read()
            raw_ids_list = get_cleaned_up_raw_ids(raw_content, self.original_separator, self.ids_type)
            self.stats['total_ids_entered'] = len(raw_ids_list)
            self.ids = set(raw_ids_list)

    def load_ids_from_gz_file(self, input_file_path):
        """
        loads the ids from a file compressed in gz format
        :param input_file_path: path where to find the file
        """
        with gzip.open(input_file_path, 'rt') as input_file:
            raw_content = input_file.read()
            raw_ids_list = get_cleaned_up_raw_ids(raw_content, self.original_separator, self.ids_type)

            self.stats['total_ids_entered'] = len(raw_ids_list)
            self.ids = set(raw_ids_list)

    def load_ids_from_zip_file(self, input_file_path):
        """
        loads the ids from a file compressed in gz format
        :param input_file_path: path where to find the file
        """
        with zipfile.ZipFile(input_file_path, 'r', zipfile.ZIP_DEFLATED, allowZip64=True) as zip_archive:
            print('Items in ZIP archive: ', zip_archive.namelist())
            file_name = zip_archive.namelist()[0]
            print('reading: ', file_name)

            with zip_archive.open(file_name, 'r') as input_file:
                raw_content = input_file.read().decode()
                raw_ids_list = get_cleaned_up_raw_ids(raw_content, self.original_separator, self.ids_type)
                self.stats['total_ids_entered'] = len(raw_ids_list)
                self.ids = set(raw_ids_list)

    def standardise_ids(self):
        """
        Standardises the ids entered with the standard separator and gets statistics about their uniqueness
        """
        self.ids = self.ids - self.ids_not_in_set
        self.standardised_raw_ids = standardisation.STANDARD_SEPARATOR.join(self.ids)
        total_unique_ids = len(self.ids)
        self.stats['total_unique_ids'] = total_unique_ids
        print('------------------------')
        print(f'total_unique_ids: {total_unique_ids}')
        print('------------------------')
        total_ids_entered = self.stats['total_ids_entered']
        print('------------------------')
        print(f'total_unique_ids: {total_unique_ids}')
        print('------------------------')
        self.stats['duplicate_ids_found'] = total_ids_entered - total_unique_ids

    def compress_ids(self):
        """
        Compresses the ids with the GZIP algorithm and encodes them in base64 as the subset generator service requires
        it
        """
        print('------------------------')
        print(f'Compressing IDs')
        print('------------------------')
        num_uncompressed_bytes = sys.getsizeof(self.standardised_raw_ids)
        self.stats['num_uncompressed_bytes'] = num_uncompressed_bytes
        gzip_compressed_ids = gzip.compress(self.standardised_raw_ids.encode())
        base64_bytes = base64.b64encode(gzip_compressed_ids)
        base64_ids = base64_bytes.decode()
        num_compressed_bytes = sys.getsizeof(base64_ids)
        self.stats['num_compressed_bytes'] = num_compressed_bytes

        self.compressed_ids = base64_ids
        self.stats['compression_ratio'] = num_compressed_bytes / num_uncompressed_bytes

    def update_ids_not_in_subset(self, ids_not_in_set):
        """
        Updates the items that should not be included in the final set
        """
        self.ids_not_in_set = ids_not_in_set
        self.standardise_ids()
        self.compress_ids()

    def append_loading_stats_to_summary(self, summary):
        """
        Appends the statistics of the loading of the ids to the summary dict passed as parameter
        :param summary: summary dict
        """
        summary['total_ids_entered'] = self.stats['total_ids_entered']
        summary['total_unique_ids'] = self.stats['total_unique_ids']
        summary['duplicate_ids_found'] = self.stats['duplicate_ids_found']

    def append_compression_stats_to_stats(self, stats):
        """
        Appends the compression statistics to the general statistics to be sent to the delayed jobs stats
        :param stats: general stats of the job
        """
        stats['num_uncompressed_bytes'] = self.stats['num_uncompressed_bytes']
        stats['num_compressed_bytes'] = self.stats['num_compressed_bytes']
        stats['compression_ratio'] = self.stats['compression_ratio']
