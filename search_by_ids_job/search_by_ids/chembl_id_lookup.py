"""
Module that performs the ChEMBL ID lookup
"""
from elasticsearch.helpers import scan

from search_by_ids_job import utils
from search_by_ids_job.search_by_ids import es_connection
from search_by_ids_job.search_by_ids import standardisation


def get_lookup_results(ids_list):
    """
    :param ids_list: list of ids for which to get the results
    :return: a dictionary with the lookup results obtained
    """
    chunk_size = 1000
    list_chunks = utils.get_list_chunks(chunk_size, ids_list)
    es_conn = es_connection.get_es_connection_from_env_config()
    lookup_results = {}
    for chunk in list_chunks:
        append_lookup_results_from_chunk(chunk, lookup_results, es_conn)

    return lookup_results


def append_lookup_results_from_chunk(chunk, lookup_results, es_conn):
    """
    :param chunk: chunk from the original list of ids
    :param es_conn: connection to elasticsearch
    :param lookup_results: lookup results structure to append results to as they are obtained
    """
    index_name = 'chembl_chembl_id_lookup'
    scanner_query = {
        "_source": ["entity_type", "status"],
        "query": {
            "terms": {
                "chembl_id": chunk
            }
        }
    }

    scanner = scan(
        es_conn,
        index=index_name,
        scroll=u'1m',
        size=1000,
        request_timeout=60,
        query=scanner_query
    )

    for doc_i in scanner:
        chembl_id = doc_i['_id']

        raw_entity_type = doc_i['_source']['entity_type']
        std_entity_type = standardisation.get_standard_entity_name_from_chembl_lookup(raw_entity_type)

        raw_id_status = doc_i['_source']['status']
        std_status = standardisation.get_standard_status_name_from_chembl_lookup(raw_id_status)

        lookup_results[chembl_id] = {
            'std_type': std_entity_type,
            'status': std_status
        }
