"""
Module that helps to report the job progress in an standardised way
"""
import sys
import json
import time
from datetime import datetime

import pytz


class JobProgressError(Exception):
    """
    Class for exceptions in this module
    """


# this is a pointer to the module object instance itself.
THIS = sys.modules[__name__]

THIS.server_connection = None

JOB_STEPS = [
    'Read input terms.',
    'Map input terms to ChEMBL IDs.',
    'Discard ChEMBL IDs that are inactive, obsolete, or from a different entity.',
    'Create results index.',
    'Generate summary files.'
]

THIS.step_times = [{} for step in JOB_STEPS]


def initialise_server_connection(server_connection):
    """
    Initialises the server connection with the one passed as parameter. It only initialises it
    if it's none
    :param server_connection: sever connection instance
    """
    if THIS.server_connection is None:
        THIS.server_connection = server_connection
    else:
        msg = "The server connection has already been initialised"
        raise RuntimeError(msg.format(THIS.db_name))


def report_step_reached(step_number, in_progress, step_progress_percentage=None):
    """
    Reports using the server connection that the step with the number passed as parameter is being
    done or completely finishes. Assumes that all the previous steps are done.
    :param step_number: number of step reported
    :param in_progress: whether it's in progress or finished
    :param step_progress_percentage: progress percentage of the step
    """
    update_step_times(step_number, in_progress)

    status_obj = generate_status_obj(step_number, in_progress, step_progress_percentage)
    num_steps_finished = step_number - 1 if in_progress else step_number
    num_total_steps = len(JOB_STEPS)
    progress_percentage = int((num_steps_finished / num_total_steps) * 100)

    status_log_msg = f'Step {step_number}: {JOB_STEPS[step_number - 1]}. {status_obj["last_step_status"]}'

    if THIS.server_connection is None:
        print('The server connection has not been set up')
        return

    THIS.server_connection.update_job_progress(
        progress_percentage=progress_percentage,
        status_log_msg=status_log_msg,
        status_description=json.dumps(status_obj)
    )


def generate_status_obj(step_number, in_progress, step_progress_percentage=None):
    """
    :param step_number: number of step reported
    :param in_progress: whether it's in progress or finished
    :param step_progress_percentage: progress percentage of the step
    :return: a status obj to send to the delayed jobs server
    """
    status_obj = {
        'all_steps': JOB_STEPS,
        'last_percentage_reported': step_progress_percentage,
        'last_step_reported': step_number,
        'last_step_status': 'IN_PROGRESS' if in_progress else 'FINISHED',
        'step_times': THIS.step_times
    }
    return status_obj


def update_step_times(step_number, in_progress):
    """
    Updates the dict that saves the times taken for every step
    :param step_number: number of step reported
    :param in_progress: whether it's in progress or finished
    """
    step_index = step_number - 1
    if in_progress:

        initial_time = time.time()
        current_step_time = {
            'initial_time': initial_time,
            'initial_time_human': datetime.fromtimestamp(initial_time).replace(tzinfo=pytz.UTC).isoformat()
        }
        THIS.step_times[step_index] = current_step_time

    else:

        current_step_time = THIS.step_times[step_index]

        initial_time = current_step_time['initial_time']
        end_time = time.time()
        current_step_time['end_time'] = end_time
        current_step_time['end_time_human'] = datetime.fromtimestamp(end_time).replace(tzinfo=pytz.UTC).isoformat()

        time_taken = end_time - initial_time
        current_step_time['time_taken'] = time_taken


def get_step_times_dict():
    """
    :return: a dictionary with the step times to make it easier to be saved in statistics
    """
    step_times_dict = {}

    step_number = 1
    for time_desc in THIS.step_times:
        step_key = f'step_{step_number}_time_taken'
        step_times_dict[step_key] = time_desc['time_taken']
        step_number += 1

    return step_times_dict
