"""
Module that handles the submission and monitoring of the subset creation requests
"""
import json
import time
import datetime

import requests

from search_by_ids_job import utils
from search_by_ids_job.search_by_ids import job_progress


class SubsetCreationRequestError(Exception):
    """
    Base class for errors in this module
    """


def get_subset_creation_submission_url():
    """
    :return: the url to use to submit the es subset generation
    """
    es_subset_generator_base_path = utils.get_es_subset_generator_base_url()
    return f'{es_subset_generator_base_path}/es_subsets/submit_subset_creation_from_compressed_ids'


def get_subset_creation_status_url(task_id):
    """
    :param task_id: the id of the task to check
    :return: the url to use to get the subset creation status
    """
    es_subset_generator_base_path = utils.get_es_subset_generator_base_url()
    return f'{es_subset_generator_base_path}/es_subsets/get_task_status/{task_id}'


def get_items_with_no_match_url(task_id):
    """
    :param task_id: the id of the task to check
    :return: the url to use to get the items that didn't match
    """
    es_subset_generator_base_path = utils.get_es_subset_generator_base_url()
    return f'{es_subset_generator_base_path}/es_subsets/get_ids_with_no_match/{task_id}'


class SubsetCreationRequest:
    """
    Class that handles a subset generation request waits, for its completion and provides results
    """

    def __init__(self, origin_index, compressed_chembl_items_ids, matches):
        self.origin_index = origin_index
        self.compressed_chembl_items_ids = compressed_chembl_items_ids
        self.matches = matches
        self.task_description = {
            'task_id': None,
            'subset_index_name': None,
            'task_status_response': None

        }
        self.summary = None
        self.items_to_review = []
        self.ids_not_in_chembl = set()

    def launch_and_wait_for_subset_creation(self, summary):
        """
        :param summary: summary dict to which to append new stats/data from this module
        :return: a dictionary with the statistics to save
        """
        job_progress.report_step_reached(step_number=4, in_progress=True)
        self.submit_subset_creation_task()
        self.wait_for_subset_to_be_ready()

        for my_summary_key, my_summary_value in self.summary.items():
            summary[my_summary_key] = my_summary_value
        job_progress.report_step_reached(step_number=4, in_progress=False)

    def submit_subset_creation_task(self):
        """
        Submits the subset creation task.
        :return: the task id
        """
        print('----------------------------')
        print('GOING TO SUBMIT REINDEXING TASK')

        submission_url = get_subset_creation_submission_url()
        print('submission_url: ', submission_url)
        payload = {
            'origin_index': self.origin_index,
            'compressed_items_ids': self.compressed_chembl_items_ids,
            'admin__test_run': utils.get_is_test_run()
        }
        print('payload:')
        print(json.dumps(payload, indent=4))
        submission_request = requests.post(submission_url, payload)
        print('submission status code: ', submission_request.status_code)
        submission_response = submission_request.json()
        print('submission response: ', submission_response)

        payload_str = json.dumps(payload)
        if len(payload_str) > 200:
            payload_str = f'{payload_str[:200]}...'
        print('payload: ', payload_str)

        task_id = submission_response['task_id']

        self.task_description['task_id'] = task_id
        print(task_id)
        print('----------------------------')

    def wait_for_subset_to_be_ready(self):
        """
        Waits for the subset creation task to be finished
        """
        print('Waiting for task to be done')
        print('task_id:')
        task_id = self.task_description['task_id']
        print(task_id)

        task_creation_url = get_subset_creation_status_url(task_id)
        print('task_creation_url: ', task_creation_url)

        task_finished = False
        previous_progress = 0
        job_progress.report_step_reached(step_number=4, in_progress=True,
                                         step_progress_percentage=previous_progress)
        while not task_finished:
            status_request = utils.get_url_with_retries(task_creation_url, num_retries=10)
            task_status_response = status_request.json()
            task_status = task_status_response['task_status']
            task_progress = int(task_status_response['progress'])
            if task_status == 'ERROR':
                raise SubsetCreationRequestError(f'The task must have not failed! But it failed! {task_creation_url}')

            now = datetime.datetime.now()
            iso_now = now.isoformat()

            print(f'{iso_now}: task_status: {task_status}, task_progress: {task_progress}')
            task_finished = task_status == 'FINISHED'

            if task_progress != previous_progress:
                job_progress.report_step_reached(step_number=4, in_progress=True,
                                                 step_progress_percentage=task_progress)
                previous_progress = task_progress

            if not task_finished:
                time.sleep(2)

        print('Subset is ready!')
        print(task_status_response)
        self.task_description['task_status_response'] = task_status_response
        self.task_description['subset_index_name'] = task_status_response['subset_index_name']

        summary = {}
        for property_name in ['origin_index', 'subset_index_name']:
            summary[property_name] = task_status_response[property_name]

        self.summary = summary
