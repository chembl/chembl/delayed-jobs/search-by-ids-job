"""
Module that handles the standarisation of different names in the job
"""
from enum import Enum


class StandardisationError(Exception):
    """
    Base class for errors with the entity name standardisation
    """


STANDARD_SEPARATOR = ','
NEW_LINE_NAME = '__NEW_LINE__'
AUTO_SEPARATOR_NAME = '__AUTO__'


class StandardEntityName(Enum):
    """
    Class that standardises the entity names for the job
    """
    COMPOUND = 'Compound'
    TARGET = 'Target'
    ASSAY = 'Assay'
    DOCUMENT = 'Document'
    CELL_LINE = 'Cell Line'
    TISSUE = 'Tissue'

    def to_string(self):
        """
        :return: the raw string representing the entity name
        """
        return str(self.value)


def get_standard_entity_name_from_chembl_lookup(raw_entity_name):
    """
    :param raw_entity_name: raw entity name from the chembl id lookup service
    :return: the standardised name for the corresponding entity
    """
    raw_name_to_entity_name = {
        'COMPOUND': StandardEntityName.COMPOUND.to_string(),
        'TARGET': StandardEntityName.TARGET.to_string(),
        'ASSAY': StandardEntityName.ASSAY.to_string(),
        'DOCUMENT': StandardEntityName.DOCUMENT.to_string(),
        'CELL': StandardEntityName.CELL_LINE.to_string(),
        'TISSUE': StandardEntityName.TISSUE.to_string()
    }
    entity_name = raw_name_to_entity_name.get(raw_entity_name)
    if entity_name is None:
        raise StandardisationError(f"I don't know which entity name to use for the raw name {raw_entity_name} "
                                   f"from the ChEMBL ID lookup")
    return entity_name


def get_standard_status_name_from_chembl_lookup(raw_status_name):
    """
    :param raw_status_name: status name from the chembl lookup id
    :return: the standardised name for the corresponding status
    """
    raw_status_name_to_standard_name = {
        'ACTIVE': 'Active',
        'OBS': 'Obsolete',
        'INACTIVE': 'Inactive'
    }
    standard_name = raw_status_name_to_standard_name.get(raw_status_name)
    if standard_name is None:
        raise StandardisationError(f"I don't know which status name to use for the raw name {raw_status_name} "
                                   f"from the ChEMBL ID lookup")
    return standard_name


def get_standard_entity_name_from_subset_index(origin_index):
    """
    :param origin_index: the name of the origin index to create the subset
    :return: the name of the entity of the subset index
    """
    index_name_to_entity_name = {
        'chembl_molecule': StandardEntityName.COMPOUND.to_string(),
        'chembl_target': StandardEntityName.TARGET.to_string(),
        'chembl_assay': StandardEntityName.ASSAY.to_string(),
        'chembl_document': StandardEntityName.DOCUMENT.to_string(),
        'chembl_cell_line': StandardEntityName.CELL_LINE.to_string(),
        'chembl_tissue': StandardEntityName.TISSUE.to_string()
    }
    entity_name = index_name_to_entity_name.get(origin_index)
    if entity_name is None:
        raise StandardisationError(f"I don't know which entity name to use for index {origin_index}")
    return entity_name
