"""
Module that provides a class that handles the connection with the delayed jobs server
"""
# pylint: disable=too-few-public-methods
import requests
import yaml


class DelayedJobsServerConnection:
    """
    Class that encapsulates functions to communicate the progress of the job to the server
    """

    def __init__(self, run_params_file_path, dry_run=False):
        """
        Constructor of the class, allows to create a connection object with the parameters provided
        :param run_params_file_path: path to the run params file
        :param dry_run: if true, do not send anything to the server
        """

        run_params = yaml.load(open(run_params_file_path, 'r'), Loader=yaml.FullLoader)

        status_update_config = run_params.get('status_update_endpoint')
        self.job_status_update_url = status_update_config.get('url')
        self.job_status_update_method = status_update_config.get('method')

        custom_statistics_config = run_params.get('custom_statistics_endpoint')
        self.custom_statistics_url = custom_statistics_config.get('url')
        self.custom_statistics_update_method = custom_statistics_config.get('method')

        self.job_token = run_params.get('job_token')
        self.dry_run = dry_run

    def update_job_progress(self, progress_percentage, status_log_msg=None, status_description=None):
        """
        Updates the job's progress percentage with the one given as parameter, sends also a msg with status_log_msg
        if you want
        :param progress_percentage: current progress percentage
        :param status_log_msg: if you want me to print a verbose output
        :param status_description: stringified dict describing the status
        :return:
        """
        print('--------------------------------------------------------------------------------------')
        print(f'Setting progress percentage to {progress_percentage}')
        print(f'Adding msg to status log: {status_log_msg}')

        url = self.job_status_update_url
        job_token = self.job_token
        headers = {
            'X-Job-Key': job_token
        }
        payload = {
            'progress': progress_percentage,
            'status_log': status_log_msg,
            'status_description': status_description
        }
        print(f'url: {url}')
        print(f'job_token: {job_token}')
        print('Headers:')
        print(headers)
        print('payload:')
        print(payload)

        print('--------------------------------------------------------------------------------------')
        if self.dry_run:
            print('Not actually sending message to the server (Dry run)')
        else:
            request = requests.patch(url, payload, headers=headers)
            print('Progress Sent to server. Response status code: ', request.status_code)

    def send_job_statistics(self, stats_dict):
        """
        Sends to the Delajed Jobs Server statistics of this job.
        :param stats_dict: dict with the stats for the job to send. See the delayed jobs documentation to know
        which is are the properties you have to send
        """
        print('--------------------------------------------------------------------------------------')
        print(f'Sending statistics. stats_obj: {stats_dict}')

        url = self.custom_statistics_url
        job_token = self.job_token
        headers = {
            'X-Job-Key': job_token
        }

        print(f'url: {url}')
        print(f'job_token: {job_token}')
        print('Headers:')
        print(headers)
        print('payload:')
        print(stats_dict)

        if self.dry_run:
            print('Not actually sending statistics to the server (Dry run)')
        else:
            request = requests.post(url, stats_dict, headers=headers)
            print('Statistics Sent to server. Response status code: ', request.status_code)
            print('Response text: ', request.text)
