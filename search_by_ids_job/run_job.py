#!/usr/bin/env python3
"""
Script that runs the search by IDs
"""
import argparse
import time
import os
import json
import traceback
import socket

import yaml

from search_by_ids_job.server_connection import DelayedJobsServerConnection
from search_by_ids_job.search_by_ids import by_ids_searcher

PARSER = argparse.ArgumentParser()
PARSER.add_argument('run_params_file', help='The path of the file with the run params')
PARSER.add_argument('-v', '--verbose', help='Make output verbose', action='store_true')
PARSER.add_argument('-n', '--dry-run', help='Do not actually send progress to the delayed jobs server',
                    action='store_true')
ARGS = PARSER.parse_args()


def run():
    """
    Runs the job
    """
    initial_time = time.time()

    print(f'Running on {socket.gethostname()}')
    run_params = load_run_params(num_retries=10)
    input_files = run_params.get('inputs')
    print('input_files: ', input_files)
    job_params = run_params.get('job_params')
    print('job_params: ', job_params)

    server_connection = DelayedJobsServerConnection(run_params_file_path=ARGS.run_params_file, dry_run=ARGS.dry_run)
    output_path = run_params.get('output_dir')

    custom_config = run_params.get('custom_job_config')
    print('custom_config: ', custom_config)
    if custom_config is not None:
        load_custom_config_as_env_variables(custom_config)

    print('RUN JOB!')
    run_stats = by_ids_searcher.do_search(
        job_params=job_params,
        input_files=input_files,
        summary_output_path=output_path,
        server_connection=server_connection
    )

    end_time = time.time()
    time_taken = end_time - initial_time
    stats_dict = {
        'time_taken': time_taken,
        **run_stats
    }
    print('------------------------------------------------------------')
    print('stats_dict')
    print(json.dumps(stats_dict, indent=4))
    print('------------------------------------------------------------')
    server_connection.send_job_statistics(stats_dict)
    server_connection.update_job_progress(100, status_log_msg='Statistics saved!')


def load_run_params(num_retries):
    """
    loads the run parameters from the yaml, retries if the file system is being annoying and says that the file is not
    found
    :param num_retries: number of retries for reading the file
    :return: the run params from the yaml from the delayed jobs
    """
    try:
        return yaml.load(open(ARGS.run_params_file, 'r'), Loader=yaml.FullLoader)
    except FileNotFoundError:
        if num_retries > 0:
            print(f'Params file not found! Maybe the file system is annoying me again. Retries left: {num_retries}')
            time.sleep(1)
            return load_run_params(num_retries-1)
        traceback.print_exc()


def load_custom_config_as_env_variables(custom_config):
    """
    Loads the custom configuration dict into the env variables
    :param custom_config: the custom configuration for the job
    """
    for key, value in custom_config.items():
        os.environ[key.upper()] = str(value)


if __name__ == "__main__":
    run()
