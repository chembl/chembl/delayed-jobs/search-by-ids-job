"""
Module with utils functions shared among other files
"""
import os
import time
import traceback

import requests


class ImproperlyConfiguredError(Exception):
    """
    Base class for errors with the configuration
    """


class ChunkSizeNonSenseError(Exception):
    """
    Class for errors with chunk sizes
    """


def get_config_env_variable(key, default_value=None):
    """
    :param key: Key of the variable to get
    :param default_value: The default value to return of the variable is not set
    :return: the environment variable required
    """
    var_value = os.environ.get(key, default_value)
    if var_value is None:
        raise ImproperlyConfiguredError(f'The {key} variable is not set!')

    return var_value


def get_es_subset_generator_base_url():
    """
    :return: the current value for the variable ES_SUBSET_GENERATOR_BASE_URL
    """
    return get_config_env_variable('ES_SUBSET_GENERATOR_BASE_URL')


def get_es_subset_generator_admin_username():
    """
    :return: the current value for the variable ES_SUBSET_GENERATOR_ADMIN_USERNAME
    """
    return get_config_env_variable('ES_SUBSET_GENERATOR_ADMIN_USERNAME')


def get_es_subset_generator_admin_password():
    """
    :return: the current value for the variable ES_SUBSET_GENERATOR_ADMIN_PASSWORD
    """
    return get_config_env_variable('ES_SUBSET_GENERATOR_ADMIN_PASSWORD')


def get_es_proxy_base_url():
    """
    :return: the current value for the variable ES_PROXY_BASE_URL
    """
    return get_config_env_variable('ES_PROXY_BASE_URL')


def get_is_test_run():
    """
    :return: the current value for the variable IS_TEST_RUN
    """
    raw_value = get_config_env_variable('IS_TEST_RUN', False)
    return bool(raw_value)


def get_list_chunks(chunk_size, original_list):
    """
    :param chunk_size: size of chunk to obtain
    :param original_list: list to split
    :return: a generator object with the chunks of the original list with the given size
    """
    if chunk_size <= 0:
        raise ChunkSizeNonSenseError(f'A chunk size of {chunk_size} does not make sense!')

    if len(original_list) == 0:
        return []

    return prepare_list_chunks_generator(chunk_size, original_list)


def prepare_list_chunks_generator(chunk_size, original_list):
    """
    Prepares the generator to get the list chunks
    :param chunk_size: size of chunk to obta
    :param original_list: list to split
    :return: a generator object with the chunks of the original list with the given size
    """

    start_index = 0
    end_index = chunk_size
    original_list_size = len(original_list)

    yield original_list[start_index:end_index]

    while end_index < original_list_size:
        start_index = end_index
        end_index += chunk_size
        yield original_list[start_index:end_index]


def get_url_with_retries(url, num_retries=10):
    """
    :param url: the url to get
    :param num_retries: the number of retries to do
    :return: the result of getting the url, if fails, it retries until the number of retries is 0
    """
    try:
        return requests.get(url)
    except requests.exceptions.ConnectionError:
        if num_retries > 0:
            print(f'Failed getting {url}. Retries left: {num_retries}')
            time.sleep(1)
            return get_url_with_retries(url, num_retries=num_retries - 1)
        traceback.print_exc()
