This project defines a job that collects a list of IDs entered by an user and orchestrates the process of mapping these
ids to a final result. It performs the following tasks

- Receives the search parameters.
- Deletes duplicates and cleans up the received input. 
- Uses the subset index generator to create the corresponding subset index. 
- Produces a summary of the search performed, the index created and the summary of the process. 

The job is expected to be run like this by the Delayed Jobs system:

```bash
/app/run_job.sh run_params.yml
```

This means that in the container, the path '/app/run_job.sh' must provide a script that receives a run parameters file
in yaml and executes the job with it.

See the tests package for examples of the input for this job.
